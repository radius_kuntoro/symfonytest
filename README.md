symfony_test
============

A Symfony test project using FOSUserBundle and YouTube API. Using Bootstrap front end.


# Installation #

1. Pull the repo
2. Run composer install
3. Run php bin/console doctrine:schema:update --force