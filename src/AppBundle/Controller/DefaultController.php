<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction(Request $request)
	{
		$user = $this->getUser();

		// replace this example code with whatever you need
		return $this->render('default/index.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
			'youtube_channel' => (!empty($user)) ? $user->getYoutubeChannel() : null,
		]);
	}

	/**
	 * @Route("/saveyoutube", name="saveyoutube")
	 */
	public function saveYoutubeAction(Request $request)
	{
		$userid = $this->getUser()->getId();
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository('AppBundle:User')->find($userid);
		
		$youtube_channel = trim($request->request->get('youtube_channel'));

		if (!$user) {
			$this->addFlash('error', 'User not found.');
		}
		elseif (empty($youtube_channel)) {
			$this->addFlash('error', 'No channel to add.');
		}

		$user->setYoutubeChannel($youtube_channel);
		$em->flush();

		$this->addFlash('success', 'Youtube link is saved.');

		return $this->redirectToRoute('homepage');
	}

	/**
	 * @Route("/hello/{name}", name="hello")
	 */
	public function helloAction($name)
	{
		return new Response('<html><body>Hello '.$name.'!</body></html>');
	}
}
