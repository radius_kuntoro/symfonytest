<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

     /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $youtube_channel;

    public function __construct()
    {
        parent::__construct();
        
    }

    /**
     * Set youtubeChannel
     *
     * @param string $youtubeChannel
     *
     * @return User
     */
    public function setYoutubeChannel($youtubeChannel)
    {
        $this->youtube_channel = $youtubeChannel;

        return $this;
    }

    /**
     * Get youtubeChannel
     *
     * @return string
     */
    public function getYoutubeChannel()
    {
        return $this->youtube_channel;
    }
}
